package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner ler = new Scanner(System.in);
        System.out.println("Teste de lógica! \n Garcons Modernos");

        System.out.println("Entrada:");

        int num = ler.nextInt();
        System.out.println("Entrada: " + num);

        System.out.println("Saida: " + AlgarismosRomanos(num));
    }

    private static String AlgarismosRomanos(int numero){

        int vlr = numero;
        String numeroRomano = "";
        String[] romanos ={"I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"};
        int[] valores ={1,4,5,9,10,40,50,90,100,400,500,900,1000};

        if (vlr < 4000) {
            for(int i=12;i>-1;i--){
                while (vlr>=valores[i]){
                    numeroRomano += romanos[i];
                    vlr -= valores[i];
                }
            }
        } else {
            System.out.print("Valor Inválido! \n");
            return Integer.toString(numero);
        }

        return numeroRomano;
    }
}
